// ==UserScript==
// @name            Simplepedia
// @version         1.1.5
// @namespace       http://userscripts.org/scripts/show/42312
// @description     MediaWiki beautification
// @author          Grant Stavely http://grantstavely.com/
// @copyright       2009+ Grant Stavely
// @license         (CC) Attribution Non-Commercial Share Alike; http://creativecommons.org/licenses/by-nc-sa/3.0/
// @include         http://*
// @include         https://*
// @exclude         *google.*
// @exclude         *userscripts.org/*
// @contributor     http://userscripts.org/users/auscompgeek
// @contributor     http://userscripts.org/users/sizzle
// @contributor     http://userscripts.org/users/67105
// @contributor     http://www.howtocreate.co.uk/bio.html
// ==/UserScript==
////////////////////////////////////////////////////////////////////////////////
// GreaseKit, Opera users: This version does not have any dependancies.
////////////////////////////////////////////////////////////////////////////////
//
alert(navigator.userAgent);
(function () {
    // User Options are below. If you are using Firefox, please use about:config to change these
    // For Greasekit users, skip down to the next section
    if (typeof GM_getValue == "function" && typeof GM_setValue == "function") {
        // Heading font?
        if (GM_getValue("headingFont") == undefined) {
            // Use the full name of the font you would like
            // I suggest 'Helvetica Neue' or 'Hoeflter Text'
            //GM_setValue("headingFont", "'Helvetica Neue', Helvetica, 'MgOpen Moderna', sans-serif");
            GM_setValue("headingFont", "'Hoefler Text', Georgia, 'Times New Roman', Times, serif");
        }
        // Here you can set the font that most of the site will use
        if (GM_getValue("bodyFont") == undefined) {
            //GM_setValue("bodyFont", "'Helvetica Neue', Helvetica, 'MgOpen Moderna', sans-serif");
            GM_setValue("bodyFont", "Georgia, Times, serif");
        }
        // Show login, edit-page, and other metadata tabs
        if (GM_getValue("user") == undefined) {
            // boolean true/false
            GM_setValue("user", false);
        }
        // Link color!
        if (GM_getValue("color") == undefined) {
            // use a css compatible value "#ff0000", "#f00", "red", and so on...
            GM_setValue("color", "#0892D0");
        }
        // Stub link color!
        if (GM_getValue("stubColor") == undefined) {
            GM_setValue("stubColor", "pink");
        }
        // Red (unwritten articles) link color!
        if (GM_getValue("newColor") == undefined) {
            GM_setValue("newColor", "red")
        }
        // Language settings are persistent, but you can give it a default if you like
        if (GM_getValue("default_language") == undefined) {
            // string value of wikipedia subdomains: http://meta.wikimedia.org/wiki/List_of_Wikipedias
            // "en", "fr", "gr", etc...
            GM_setValue("default_language", "en");
        }
        // Show a selection list of all alternate language versions of any document
        // Wikipedia specific
        if (GM_getValue("international") == undefined) {
            // boolean true/false
            GM_setValue("international", true);
        }
        // Now throw all our prefs into an object
        var prefs = {
            headingFont: GM_getValue("headingFont"),
            bodyFont: GM_getValue("bodyFont"),
            user: GM_getValue("user"),
            color: GM_getValue("color"),
            stubColor: GM_getValue("stubColor"),
            newColor: GM_getValue("newColor"),
            defaultLanguage: GM_getValue("default_language"),
            international: GM_getValue("international")
        }
    } else {
        // Omniweb, Safari, etc users:
        var prefs = {
            // headingFont: "'Helvetica Neue', Helvetica, 'MgOpen Moderna', sans-serif",
            headingFont: "'Hoefler Text', Georgia, 'Times New Roman', Times, serif",
            // bodyFont: "'Helvetica Neue', Helvetica, 'MgOpen Moderna', sans-serif",
            bodyFont: "Georgia, Times, serif",
            user: true,
            color: "#0892D0",
            stubColor: "pink",
            newColor: "red",
            defaultLanguage: "en",
            international: true
        }
    }
    // hard code preference overrides
    // prefs.international = true;
    ////////////////////////////////////////////////////////////////////////////////
    var wiki = false;
    // look for obvious "I was made by mediawiki" first
    var metadata = document.getElementsByTagName("meta");
    for (var i = metadata.length -1; i >= 0; i--) {
        var generator = metadata[i].getAttribute("content");
        if (generator.match(/mediawiki/i)) {
            wiki = true;
        }
    }
    if (!wiki) {
        // only stupid with it if necessary
        if (location.href.match(/wiki/i) || location.href.match(/http:\/\/?(www\.|)wikipedia\.org\//i) ) { // The site says that we're in a wiki
            wiki = true;
        }
        // and stupider
        else { 
            var cssLinks = document.getElementsByTagName("link");
            for (var i = cssLinks.length -1; i >= 0; i--) {
                var cssLink = cssLinks[i].getAttribute("href");
                if (cssLink.match(/monobook/i)) {
                    wiki = true;
                }
            }
        }
        // and stupiderer
        else {
            var inlineStyle = document.getElementsByTagName("style");
            for (var i = inlineStyle.length -1; i >= 0; i--) {
                var style = inlineStyle[i].childNodes[0].data;
                if (style.match(/monobook|wiki/i)) {
                    wiki = true;
                }
            }
        }
    }
    if (!wiki) return; // Make sure we only run on wikis
    ////////////////////////////////////////////////////////////////////////////////
    // Make sure that we can style everything properly.
    if (typeof GM_addStyle != "function") {
        function GM_addStyle(css) {
            if (typeof addStyle == "function") {
                addStyle(css);
            } else if (typeof PRO_addStyle == "function") {
                PRO_addStyle(css);
            } else {
                var heads = document.getElementsByTagName("head");
                if (heads.length > 0) {
                    var node = document.createElement("style");
                    node.type = "text/css";
                    node.appendChild(document.createTextNode(css));
                    heads[0].appendChild(node);
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////
    // Away we go
    try {
        GM_addStyle("/*------------------------------------*RESET*------------------------------------*//*http://meyerweb.com/eric/tools/css/reset/v2.0b1|201101NOTE:WORKINPROGRESSUSEWITHCAUTIONANDTESTWITHABANDON*/html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,figcaption,figure,footer,header,hgroup,menu,nav,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;font:inherit;vertical-align:baseline;}/*HTML5display-roleresetforolderbrowsers*/article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block;}body{line-height:1;}ol,ul{list-style:none;}blockquote,q{quotes:none;}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none;}/*remembertodefinevisiblefocusstyles!:focus{outline:?????;}*//*remembertohighlightinsertssomehow!*/ins{text-decoration:none;}del{text-decoration:line-through;}table{border-collapse:collapse;border-spacing:0;}/*FANCYEXPERIMENTALCSS*/{text-rendering:optimizeLegibility;}/*------------------------------------*MAIN*------------------------------------*//*GO!*//*disapearstuff*/#coordinates,#p-gigyaapplet,#p-sharethis,#p-,#p-navigation,#p-interaction,#p-feedback,#p-search,#donate,#p-lang,#anontip,#anon-banner,#mp-banner,#filetoc,#No_article_title_matches,#msg-noexactmatch,#mp-strapline,#footer,#siteSub,#p-search,#p-navigation,#p-logo,#p-interaction,#p-tb,#p-lang,#p-scholarpedia,#p-encyclopedias,#f-list,#f-poweredbyico,#f-copyrightico,#mw_header,#siteNotice,#mr_banner,#mr_banner_topad,#mr_header,#guides,#background_strip,#wikia_header,#widget_sidebar,#monaco_footer,#LEFT_SKYSCRAPER_2_load,#LEFT_SKYSCRAPER_3_load,#LEFT_SKYSCRAPER_1_load,#TOP_RIGHT_BOXAD_load,#page_bar,#google_ads_div_LEFT_SPOTLIGHT_1,#ads,#mw-panel,#mw-head,#mw-page-base,#mw-head-base,.portleth5,.hiddeninputs,.metadata,.centralauth-login-box,.generated-sidebar,.mbox-image,.floatleft,.floatright{display:none!important;}/*betawikipediastuff*/#vectorTabs,#p-coll-create_a_book{dislay:none;}/*LAYOUT*/#wrapper{position:absolute;left:50%;top:0px;width:850px;height:100%;margin-left:-425px;}#navigation{position:absolute;top:0px;width:150px;background:#fff;text-align:left;height:100%;top:7.25em;}#microblog{position:absolute;right:0px;width:200px;height:100%;top:4.5em;}#microblogp{padding-top:.75em;}#navigationp,#microblogp,#microblogblockquote,#microblogul,#microblogol,#microblogli{font-size:.85em;}#content{position:absolute;width:400px;height:100%;left:200px;top:2.75em;}#contentimg{width:80%;}#search{text-align:center;padding-top:20px;}.title{color:#000!important;font-style:normal!important;}.small-print{padding-top:5px;color:#333;padding-bottom:30px;line-height:1.5em;}.googlenav{display:inline;}#googlenav{text-align:center;}body{color:#000;background-color:white;font-family:georgia,sans-serif;font-size:13px;line-height:1.5;}a:link,a:visited,a:active{text-decoration:none;/*color:http://en.wikipedia.org/wiki/Electric_blue_(color);*/color:#0892D0;font-style:italic;}.comment{padding-bottom:2em;line-height:1.5;}.txpCommentInputMessage,.comment_web_input,.comment_email_input,.comment_name_input,.searchinput{padding:1px;}.trajan{font-family:'TrajanPro',Trajan,'TimesNewRoman';}.console{font-family:'DroidSansMono','LucidaConsole',monospace;}.prompt{color:#0892D0;}.fancy{font-style:italic;color:#666;}a:hover{color:#000;-webkit-transition:color.25slinear;transition:color.25slinear;}a:active:{color:#000;}pre{width:400px;overflow:auto;}code{display:inline;color:#333;background-color:#fff;font:monospace;}p{line-height:1.5;margin-top:1.5em;margin-bottom:0em;}/*Paragraphindenting*/#contentp,.post,br+br{text-indent:1em;margin-top:0}#microblogp+p,#navigationp+p,h1+p,h2+p,h3+p,h4+p,blockquote+p,hr+p,ul+p,ol+p,img+p+p,div{text-indent:0!important;}/*Styleguide*/h1,h2,h3,h4,h5,h1a,h2a,h3a{color:#000;font-weight:normal;text-align:center;}h1{font-size:2.25em;}h2{font-size:1.5em;}h3{font-size:1.125em;}h4{font-size:1em;line-height:2.5;}hr{border:none;}blockquote{margin:1.5em;font-style:italic;line-height:1.5;}sup,sub{height:0;line-height:1;vertical-align:baseline;_vertical-align:bottom;position:relative;}sup{bottom:1ex;}sub{top:.5ex;}.posted{text-align:right!important;font-style:italic;margin-top:-.75em;}#navigationul,#navigationol,.titles,.postul{list-style-type:none;}li+li{padding-top:.5em;}li{margin-left:2em;}ol,ul{padding-top:2em;padding-bottom:2em;}del{color:#999;}.pages_nav{clear:both;}.previewpost{border-left:20pxsolid#781600;background-color:#fff;color:#000;}.imred{color:#c90130;font-weight:bold;}.imgreen{color:#7da323;font-weight:bold;}.imblue{color:#00a7d2;font-weight:bold;}.console{font:11px'AndaleMono','LucidaConsole',courier,'timesnewroman';}");
    }
    catch(err) {}

    // Add font option styles
    if (prefs.headingFont && prefs.bodyFont) {
        try {
            GM_addStyle("h1,h2,h3,h4,h5#mp-tfp-h2,.mp-header,#mp-tfp-h2,.mw-headline{font-family:" + prefs.headingFont + "!important;}html,p,body,#globalWrapper{font-family:" + prefs.bodyFont + "!important;}");
        }
        catch(err) {}
    }

    // Add link colors
    if (prefs.color) {
        try {
            GM_addStyle(".toctext,.toclinks,.toclevel-1,a:link,a:visited,a:active{font-weight:normal;text-decoration:none;color:" + prefs.color + "!important;}");
        }
        catch(err) {}
    }
    // Add link colors for unrwritten articles
    if (prefs.newColor) {
        try {
            GM_addStyle("a:link.new,a:active.new,a:visited.new{color:" + prefs.newColor + "!important;}");
        }
        catch(err) {}
    }

    // Hide all logged in user commands
    if (!prefs.user) {
        try {
            GM_addStyle("#login,#pt-login,#edit-ul,#login-ul,#user-ul,.editsection{display:none!important;}");
        }
        catch(err) {}
    }

    // Give the article editing ul an id
    try {
        var editNav = document.getElementById("ca-nstab-main");
        editNav.parentNode.setAttribute("id", "edit-ul");
    }
    catch(err) {}

    // Front-page specific change for WikipediA only
    if (location.href.match(/http:\/\/?(www\.|)wikipedia\.org\//i)) {
    //if (location.href.match(/http:\/\/?(www\.)?wikipedia\.org\//i)) { //which one is more efficient?
        var newFrontPage = document.createElement("div");
        newFrontPage.innerHTML='\
            <div id="logotype">\
                <a href="http://' + prefs.defaultLanguage + '.wikipedia.org/">\
                    <div id="frontlogo">Wikipedia</div>\
                </a>\
            </div>\
            <div id="globeLogo">\
                <div id="front_search_bar">\
                    <form id="front-search" action="/w/index.php" method="get">\
                        <input name="title" type="hidden" value="Special:Search" />\
                        <input name="ns0" type="hidden" value="1" />\
                        <input id="frontsearchInput" name="search" type="text" tabindex="1" title="Search Wikipedia [f]" accesskey="f" value="" />\
                        <br />\
                        <input type="submit" name="submit" value="Search Wikipedia" />\
                    </form>\
                    <form id="random-article" action="/wiki/Special:Random">\
                        <input type="submit" name="random" value="I\'m Feeling Lucky" />\
                    </form>\
                </div>\
            </div>';
        try {
            newFrontPage.setAttribute('id','NewFront');
            var oldFrontPage = document.getElementById("bodyContent");
            oldFrontPage.parentNode.replaceChild(newFrontPage, oldFrontPage);
        }
        catch(err) {}
    }
    // Add a new favicon if it is wikipedia
    if (location.href.match(/http:\/\/?(\w*\.|)wikipedia\.org\//i)) {
    //if (location.href.match(/http:\/\/?(\w*\.)?wikipedia\.org\//i)) { //which one is more efficient?
        var icon = document.createElement("link");
        icon.setAttribute("type", "image/x-icon");
        icon.setAttribute("rel", "shortcut icon");
        icon.setAttribute('href', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAALpJREFUeNrEU1ENhTAMHDjAwiw8C7PwLGABLWhAAlhAwpAwJAyaXJdbs7z3wQdNmjVlbe96o8s5uyfWu4f2fgOHHUQJbx/p04xcpJxHTtxLrSLYcH7ocqAib3IHvDTYfxRxXgcslkIFDVQkTjhnXFeqodTSO+A9KP+J9uCpaRnOKiwEM1DuQPHX0K0oKE+GHY0aiVA1EYgS5+2DUUYnDv8QiK20zLGhfbK19iVujbhoXskH617/Gy8BBgDHv1uI+O2cEwAAAABJRU5ErkJggg======');
        var head = document.getElementsByTagName("head")[0];
        head.appendChild(icon);
    }
    // Replace the main page header
    var newHeaderContent = document.createElement("h1");
    newHeaderContent.innerHTML="\
        Wikipedia\
            ";
    try {
        newHeaderContent.setAttribute("id","firstHeading")
        var oldheader = document.getElementById("mp-topbanner");
        oldheader.parentNode.replaceChild(newHeaderContent,oldheader);
    }
    catch(err) {}

    // Create a floating search and language jump nav element
    // Search using whatever search form the page already has
    try {
        var searchform = document.getElementById("searchform").getAttribute("action");
            var searchbar = document.createElement("div");
            searchbar.innerHTML='\
                    <form id="search" action="' + searchform + '" method="get">\
                            <div id="search_bar">\
                                <input name="title" type="hidden" value="Special:Search" />\
                                <input name="ns0" type="hidden" value="1" />\
                                <input id="searchInput" name="search" type="text" title="Search Wikipedia [f]" accesskey="f" value="" />\
                            </div>\
                    </form>';
            searchbar.id="floatingsearch";
            document.getElementById("content").appendChild(searchbar);
    }
    catch(err) {}

    // Jump to the same article in other languages
    // This mostly works but doesn't jump on change,so I'm not using it yet
    if (prefs.international) {
        try {
            var langList = document.getElementById("p-lang").getElementsByTagName("a");
            if (langList.length > 0) {
                var langJump = document.createElement("select");
                langJump.id = "langJump";
                langJump.name = "langJump";
                var langList = document.getElementById("p-lang").getElementsByTagName("a");
                var langTitle = document.createElement("option");
                // snag 'in another language' in the local translation
                langTitle.text = document.getElementById("p-lang").getElementsByTagName('h5')[0].innerHTML;
                langJump.add(langTitle,null);
                for (var i = 0; i <= langList.length -1; i++) {
                    var langOpt = document.createElement("option");
                    langOpt.text = langList[i].innerHTML;
                    langOpt.value = langList[i];
                    try {
                        langJump.add(langOpt,null);
                    }
                catch(err) {}
                }
                langJump.addEventListener("change", function() {
                    window.location.href = this.options[this.selectedIndex].value;
                }, false);
                try {
                    document.getElementById("content").appendChild(langJump);
                }
                catch(err) {}
            }
        }
        catch(err) {}
    }

    try {
        // pt-userpage is only there if already logged in
        var userNav = document.getElementById("pt-userpage");
        userNav.parentNode.setAttribute("id","user-ul");
        // when not, use pt-login
        var userLogin = document.getElementById("pt-login");
        userLogin.parentNode.parentNode.setAttribute("id","login");
        userLogin.parentNode.parentNode.setAttribute("class","");
    }
    catch(err) {}
    ////////////////////////////////////////////////////////////////////////////////
    // Auto-updating for Firefox users
    CheckScriptForUpdate = {
        days: 7, // Days to wait between update checks
        id: "42312",
        time: new Date().getTime() | 0,
        name: "Simplepedia",
        call: function(response) {
            GM_xmlhttpRequest({
                method: "GET",
                    url: "https://userscripts.org/scripts/source/" + this.id + ".meta.js",
                    onload: function(xpr) {CheckScriptForUpdate.compare(xpr, response)}
            });
        },
        compare: function(xpr, response) {
            this.xversion=/\/\/\s*@version\s+(.*)\s*\n/i.exec(xpr.responseText);
            this.xname=/\/\/\s*@name\s+(.*)\s*\n/i.exec(xpr.responseText);
            if (this.xversion && this.xname[1] == this.name) {
                this.xversion = this.xversion[1];
                this.xname = this.xname[1];
            } else {
                if (xpr.responseText.match("Uh-oh! The page could not be found!") || (this.xname[1] != this.name)) GM_setValue("updated", "off");
                return false;
            }
            this.xversionParts = this.xversion.split(".");
            this.versionParts = this.version.split(".");
            while ((this.xversionParts.length > 0) && (this.versionParts.length > 0))
            {
                if (this.xversionParts[0] < this.versionParts[0])
                    this.newerVersion = -1;
                if (this.xversionParts[0] > this.versionParts[0])
                    this.newerVersion = 1;
                this.xversionParts.shift();
                this.versionParts.shift();
            }
            if (this.xversionParts.length > 0)
                this.newerVersion = -1;
            if (this.versionParts.length > 0)
                this.newerVersion = 1;
            this.newerVersion = 0;
            if (this.newerVersion == 1 && confirm("A new version of the " + this.xname + " user script is available. Do you want to update?")) {
                GM_setValue("updated", this.time);
                GM_openInTab("http://userscripts.org/scripts/source/" + this.id + ".user.js");
            } else if (this.newerVersion == 1) {
                if (confirm("Do you want to turn off auto updating for this script?")) {
                    GM_setValue("updated", "off");
                    GM_registerMenuCommand("Auto Update " + this.name, function() {GM_setValue("updated",new Date().getTime() | 0); CheckScriptForUpdate.call("return")});
                    alert("Automatic updates can be re-enabled for this script from the User Script Commands submenu.");
                } else {
                    GM_setValue("updated", this.time);
                }
            } else {
                if (response) alert("No updates available for " + this.name);
                GM_setValue("updated", this.time);
            }
        },
        check: function() {
            if (GM_getValue("updated", 0) == 0) GM_setValue("updated", this.time);
            if (GM_getValue("updated", 0) != "off" && +this.time > (+GM_getValue("updated", 0) + (1000*60*60*24*this.days))) {
                this.call();
            } else if (GM_getValue("updated", 0) == "off") {
                GM_registerMenuCommand("Enable " + this.name + " updates", function() {GM_setValue("updated", new Date().getTime() | 0); CheckScriptForUpdate.call(true)});
            } else {
                GM_registerMenuCommand("Check " + this.name + " for updates", function() {GM_setValue("updated", new Date().getTime() | 0); CheckScriptForUpdate.call(true)});
            }
        },
    };
    if (self.location == top.location && GM_xmlhttpRequest) CheckScriptForUpdate.check();
})();
/** Change Log
 * Version 1.1.5 July 12, 2010
    - Integrated auscompgeeks 1.1 branch with minor compatibility changes
    - Updated css to work with Wikipedia's current theme
    - Browser detection currently borked
    - Prefs currently slightly borked

 * Version 1.1 June 30, 2010
    - Split the version number and compare each individually, instead of
        stripping dots out of the version in the update check
    - Embed the favicon into the script instead of fetching it from a server

 * Version 1.0 May 6, 2010
    - Automatically detect non-Gecko browsers and act accordingly
    - Attempt GM_getValue() and GM_setValue() before checking for wikis only on Gecko browsers
    - Store prefs in an object
    - Removed extraneous whitespace in CSS
    - Improved readability of auto-update code, sorry sizzlemctwizzle...
    - Squashed a few pref bugs
    - Add new prefs: newColor (new article links), stubColor (stub article links)

 * Version .992 September 11, 2009
    - Enclosed all of Simplepedia in an unnamed function
    - Don't execute on Google
    - Don't attempt GM_getValue() or GM_setValue() if the site isn't a wiki
      (for Opera/GreaseKit users who are lazy)

 * Version .991 July 17, 2009
    - Added a W favicon for wikipedia only
    - Cleaned preference handling a bit, please reset and reload to use
    - Fixed display of international language selection to use a localized title
    - Darkened the darks a bit
    - Misc. CSS tweaking
    - Updated wikipedia discovery to regex better

 * Version .99 July 15, 2009
    - Fixed the alternate language select drop-down and made it an option

 * Version .983 July 10, 2009
    - Miscelaneous small CSS fixes adapting to changes made at Wikipedia
    - Simplified front page further
    - Made en.wikipedia closer resemble artile pages
    - Embracing helvetica, possibly renaming to helvetipedia soon

 * Version .982 June 22, 2009
    - Tweaked thumbnail picture padding to correct a hover issue (thanks sdfghrr)
    - Tweaked Encyclopedia Dramatica again

 * Version .98 June 2, 2009
    - Added a new about:config / cookie option to make customizing link colors more discoverable

 * Version .97 June 2, 2009
    - Resolved issues with preference handling in webkit
    - Added disabled preference to dynamically create jump-list of all alternate
      language versions of a given document on wikipedia

 * Version .96
    - Generic wiki export has been expanded to specifically support wikia.com,
      while generically supporting any other site with 'wiki' in the CSS @import

 * Version .95 May 18, 2009
    - Improved font selection changes and examples
    - Updated namespace
    - Fixed Auto-updating menu selection controls

 * Version .94 May 16, 2009
    - Reset font selection to allow greater user control
    - Tweaked JavaScript style, thanks iandalton
    - Updated CSS for multiple fixes

 * Version .93 May 14, 2009
    - Added checks to leave user configured items alone, thanks iandalton
    - Updated CSS for multiple fixes

 * Version .92 May 10, 2009
    - Complete rewrite
    - Removed document title adjustment
    - Removed specific site support
    - Added generic MediaWiki detection
    - Added proper search form submission detection
    - Began using GreaseMonkey API - Mac users will need secondary scripts now

 * Version .9.1 April 30, 2009
    - Fixed front page WikipediA logo (Wikipedia moved it)
    - Dumped Firefox specific auto-updater in favor of pure JS version by Jarett
      http://userscripts.org/scripts/show/20145
    - Fixed http/https mixup when browsing secure sites, Simplepedia will now
      also use https to grab external CSS
    - Updated front page bookshelves to link to random pages because it makes more sense to me

 * Version .9 April 29, 2009
    - Added auto-updating via Another Auto Update Script by sizzlemctwizzle
      http://userscripts.org/scripts/show/38017
    - Restored TOC toggle for auscompgeek

 * Version .8.1.51 April 13, 2009
    - Added support for http://*.intelink.gov/wiki/*, just in case

 * Version .8.1 April 11, 2009
    - Added a simple wikipedia graphic anchor for / by the search bar
    - Swapped the same out on wikipedia.org/
    - Added support for http://wiki.greasespot.net/*
    - Improved user/editor display option

 * Version .8 April 5, 2009
    - Added preliminary support for many more wikis http://en.wikipedia.org/wiki/List_of_wikis
      with main page bugs calling them all WikipediA

 * Version .7.5 March 30, 2009
    - Reintroduced edit links, page and user login tabs, ++

 * Version .7.2.1 March 25, 2009
    - Added helper functions, basic error checking

 * Version .7.2 March 18, 2009
    - Added basic support for wikileaks.org
    - Added support for secure WikiMedia sites

 * Version .7.1 March 17, 2009
    - Fixed front page form elements in Firefox
    - Added default language links to the front page

 * Version .7 March 17, 2009
    - Added 'I'm feeling lucky' and 'Search' buttons to the front portal
    - Reintroduced .noprint content for the main pages
    - Fixed center td border display
*/
