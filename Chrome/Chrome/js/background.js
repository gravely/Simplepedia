// Set up the omnibox
// This event is fired each time the user updates the text in the omnibox,
// as long as the extension's keyword mode is still active.
chrome.omnibox.onInputChanged.addListener(
  function(text, suggest) {
    console.log('inputChanged: ' + text);
    suggest([
      {content: text + 'Search from Simplepedia, ', description: 'continue entering search terms to search Wikipedia.'},
      //{content: text + " number two", description: "the second entry"}
    ]);
  });

// This event is fired with the user accepts the input in the omnibox.
chrome.omnibox.onInputEntered.addListener(
  function(text) {
    console.log('inputEntered: ' + text);
    url = 'http://' + localStorage['default_language'] + '.wikipedia.org/w/index.php?title=Special%253ASearch&ns0=1&search=' + encodeURIComponent(text);
    chrome.tabs.getSelected(null, function(tab) {
        chrome.tabs.update(tab.id, { url:url });
    });
  });
// Called when a message is passed.  We assume that the content script
// wants to show the page action.
function onRequest(request, sender, sendResponse) {
    console.log('request received: ' + request.method);
    if (localStorage[request.method]) {
        console.log('Asked for: ' + request.method + ' and returned ' + localStorage[request.method]);
        sendResponse({status: localStorage[request.method]}); 
    }
    else {
        // Show the page action for the tab that the sender (content script)
        // was on.
        chrome.pageAction.show(sender.tab.id);
        // return the page to normal display
        $('body').css('display', 'block');
    }
}

// Listen for the content script to send a message to the background page.
chrome.extension.onRequest.addListener(onRequest);
