// Saves options to localStorage.
function save_options() {
    var settings = document.getElementById("features").getElementsByTagName("input");
    var fonts = document.getElementById("style").getElementsByTagName("select");
    var colors = document.getElementById("style").getElementsByTagName("input");

    // walk feature settings and save each
    for (var i = 0; i <= settings.length - 1; i++) {
        localStorage[settings[i].id] = settings[i].checked;
        console.log("Saving [" + settings[i].id + "] = " + localStorage[settings[i].id]);
    }
    // walk style settings and save each in as style objects
    for (var i = 0; i <= fonts.length - 1; i++) {
        for (var j = 0; j < fonts[i].children.length; j++) {
            var child = fonts[i].children[j];
            if (child.selected == true) {
                localStorage[fonts[i].id] = child.value;
                console.log("Setting [" + fonts[i].id + "] = " + child.value);
            }
        }
    }
    for (var i = 0; i <= colors.length - 1; i++) {
        localStorage[colors[i].id] = colors[i].value;
        console.log("Setting [" + colors[i].id + "] = " + colors[i].value);
    }

    // Update status to let user know options were saved.
    var status = document.getElementById("status");
    status.innerHTML = "Options Saved.";
    setTimeout(function() {
        status.innerHTML = "";
        }, 750);
}

function tease() {
    $('status').text("Good choice.");
    setTimeout(function() {
            $('status').text('');
            }, 750);
}

// Restores options to saved value from localStorage.
function restore_options() {
    var settings = document.getElementById("features").getElementsByTagName("input");
    var fonts = document.getElementById("style").getElementsByTagName("select");
    var colors = document.getElementById("style").getElementsByTagName("input");

    // walk feature settings and restore each
    for (var i = 0; i <= settings.length - 1; i++) {
        console.log(settings[i].id + " = " + localStorage[settings[i].id]);
        if (localStorage[settings[i].id] == "false") {
            settings[i].checked = false;
        }
        else {
            settings[i].checked = localStorage[settings[i].id];
        }
    }
    // walk style settings and restore each
    for (var i = 0; i <= fonts.length - 1; i++) {
        for (var j = 0; j < fonts[i].children.length; j++) {
            var child = fonts[i].children[j];
            console.log("Comparing " + child.value + " to " + localStorage[fonts[i].id]);
            if (child.value == localStorage[fonts[i].id]) {
                child.selected = true;
                console.log(fonts[i].id + "= " + localStorage[fonts[i].id]);
                break;
            }
        }
    }
    for (var i = 0; i <= colors.length -1; i++) {
        colors[i].value = localStorage[colors[i].id];
        $('#' + colors[i].id).css('background-color', localStorage[colors[i].id]);
        $('#' + colors[i].id).css('color','black'); 
        console.log(colors[i].id + " = " + localStorage[colors[i].id]);
    }
}


function restore_defaults() {
    localStorage["heading_font"] = "'Hoefler Text', Times New Roman, Times, serif";
    localStorage["copy_font"] = "Georgia, Palatino,' Palatino Linotype', Times, 'Times New Roman', serif";
    localStorage["user"] = false;
    localStorage["edit"] = false;
    localStorage["bg_color"] = "#ffffff";
    localStorage["hover_color"] = "#000000";
    localStorage["active_color"] = "#000000";
    localStorage["text_color"] = "#000000";
    localStorage["headings_color"] = "#000000";
    localStorage["link_color"] = "#0892D0";
    localStorage["visited_color"] = "#0892D0";
    localStorage["stub_color"] = "#FF3729";
    localStorage["new_color"] = "#FF3729"
    localStorage["default_language"] = "en";
    localStorage["international"] = false;
    restore_options();
    applyStyle();
}
