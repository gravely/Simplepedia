// Kick on the omnibox icon
chrome.extension.sendRequest({}, function(response) {});

////////////////////////////////////////////////////////////////////////////////
// Away we go

// Remove all existing stylesheets
$('link[rel="stylesheet"]').remove();
$('style').remove();

// Remove all inline style 
// (with known exceptions)
$('*:not(.thumbinner,.toccolours,.toccolours div,.roccolours,.roccolours div)').removeAttr('style');
$('*').removeAttr('color');
$('*').removeAttr('width');
$('*').removeAttr('height');
$('*').removeAttr('font');

// Remove non-content and competing scripts
$('script').remove();
$('iframe').remove();
$('.suggestions').remove();
$('#mw-js-message').remove();
$('#sitenotice').remove();
$('#mw-panel').remove();
$('#left-navigation').remove();
$('#right-navigation').remove();
$('#footer').remove();
$('#siteSub').remove();
$('#sitesub').remove();
$('#jump-to-nav').remove();
$('.printfooter').remove();
$('.printonly').remove();
$('.internal').remove();
$('#coordinates').remove();
$('.collapseButton').remove();
$('.mbox-image').remove();
$('.protected-icon').remove();
$('.topicon').remove();

// If it shouldn't be printed it shouldn't be read
$('.noprint').remove();
// oddballs
$('img[title="close"]').remove();

// Front-page specific change for WikipediA only
if (location.href.match(/http:\/\/?(www\.|)wikipedia\.org\//i)) {
    var searchform = $('#searchform')[0].outerHTML;
    var newFrontPage = document.createElement("div");
    newFrontPage.innerHTML='\
        <div id="logotype">\
                <div id="frontlogo">Wikipedia</div>\
        </div>\
        <div id="globeLogo">\
            <div id="front_search_bar">' + searchform + '\
            </div>\
        </div>';
    newFrontPage.setAttribute('id','NewFront');
    try {
        $('#bodyContent').replaceWith($(newFrontPage));
        $('#searchInput').attr('size', '60');
        $('#searchInput').after('<br /><form id="random-article" action="/wiki/Special:Random"><input type="submit" name="random" value="?" /></form>');
    }
    catch(err) {console.log(err);}
}
// everything else should only be done on article pages
else {
    // in which we directly attack specific popular wikimedia templates, for lack of a 
    // better generic strategy:
    // Edit links: "[" <- really?
    try {
        var editsections = ($('span.editsection').get());
        for (var i = 0; i < editsections.length; i++) {
            var links = ($('a', editsections[i]).get());
            for (var j = 0; j < links.length; j++) {
                $(editsections[i]).replaceWith('<a class="editsection" href="' + $(links[j]).attr('href') + '">✎</a>');
            }
        }
    }
    catch(err) {console.log(err);}

    // Eliminate anything similar to cquote. This entire script could be called cquote.
    // cquote is everything wrong with the style of wikipedia in one, ugly, terrible
    // template. So. Ugly. 
    try {
        var cells = ($('td').get());
        for (var i = 0; i < cells.length; i++) {
            if ($(cells[i]).text() == '“') {
                // AH HA! A Table cell that only exists to add a purple curly quote!
                console.log('Replacing a cquote with a blockquote containing: ' + $(cells[i + 1]).text());
                // replace the monster's parent table, thus restoring sanity to the world,
                // with a blockquote containing the next td's content
                $(cells[i]).closest('table').replaceWith('<blockquote>' + $(cells[i + 1]).text() + '</blockquote>');
            }
            //alert("Orchestra hit!");
        }
    }
    catch(err) {console.log(err);}
    
    // clean up reference sups
    try {
        var spans = ($('a span').get());
        for (var i = 0; i < spans.length; i++) {
            if ($(spans[i]).text() == '[' || ']') {
                // AH HA! A Table cell that only exists to add a purple curly quote!
                //console.log('Fixing reference sups for ref: ' + $(spans[i + 1]).text());
                $(spans[i]).text('');
            }
        }
        var sups = ($('sup a').get());
        for (var i = 0; i < sups.length; i++) {
            var sup = $(sups[i]).text();
            sup.replace(']', '');
            sup.replace('[', '');
            $(sups[i]).text(sup);
        }
    }
    catch(err) {console.log(err);}
       
    // nix the hard-coded css columns for references: ugh 
    try {
        $('.reflist').removeAttr("style");
        $('.references-column-width').removeAttr("style");
    }
    catch(err) {console.log(err)};

    // Begin adding and rearranging:
    //
    // jump to headings with vi shortcuts
    try {
        function next_heading(){
            console.log("j pressed.");
        }
        function previous_heading(){
        }
        function last_heading(){
        }
        function first_heading(){
        }
        function scroll_eof(){
        }

        $(document).bind('keydown', 'j', next_heading);
        $(document).bind('keydown', 'k', previous_heading());
        $(document).bind('keydown', 'L', last_heading());
        $(document).bind('keydown', 'H', first_heading());
        $(document).bind('keydown', 'G', scroll_eof());

    }
    catch(err) {console.log(err)};
    //
    // Search using whatever search form the page already has
    try {
        // so's we stay on the same language 
        var searchform = $('#searchform').attr('action');
            // build it
            var searchbar = document.createElement('div');
            searchbar.innerHTML='\
                    <form id="search" action="' + searchform + '" method="get">\
                            <div id="search_bar">\
                                <input name="title" type="hidden" value="Special:Search" />\
                                <input name="ns0" type="hidden" value="1" />\
                                <label for="searchinput">Search:</label>\
                                <input id="searchInput" name="search" type="text" name="searchinput" title="Search Wikipedia [f]" accesskey="f" results="10" value="" />\
                                <br />\
                             </form>\
                            </div>\
                    </form>';
            searchbar.id="floatingsearch";
            $('#bodyContent').append($(searchbar));
    }
    catch(err) {console.log(err);}

    // Jump to the same article in other languages
    if (localStorage['international']) {
        try {
            var langList = document.getElementById("p-lang").getElementsByTagName("a");
            if (langList.length > 0) {
                var langJump = document.createElement("select");
                langJump.id = "langJump";
                langJump.name = "langJump";
                var langList = document.getElementById("p-lang").getElementsByTagName("a");
                var langTitle = document.createElement("option");
                // snag 'in another language' in the local translation
                langTitle.text = document.getElementById("p-lang").getElementsByTagName('h5')[0].innerHTML;
                langJump.add(langTitle,null);
                for (var i = 0; i <= langList.length -1; i++) {
                    var langOpt = document.createElement("option");
                    langOpt.text = langList[i].innerHTML;
                    langOpt.value = langList[i];
                    try {
                        langJump.add(langOpt,null);
                    }
                catch(err) {console.log(err);}
                }
                langJump.addEventListener("change", function() {
                    window.location.href = this.options[this.selectedIndex].value;
                }, false);
                try {
                    document.getElementById("content").insertBefore(langJump);
                }
                catch(err) {console.log(err);}
            }
        }
        catch(err) {console.log('Unable to find this article in any other lanuages.');}
    }

    try {
        // pt-userpage is only there if already logged in
        var userNav = document.getElementById("pt-userpage");
        userNav.parentNode.setAttribute("id","user-ul");
        // when not, use pt-login
        var userLogin = document.getElementById("pt-login");
        userLogin.parentNode.parentNode.setAttribute("id","login");
        userLogin.parentNode.parentNode.setAttribute("class","");
    }
    catch(err) {console.log('You don\'t appear to be logged into Wikipedia.');}
    $(document).ready( function() {
            console.log('DOM ready.');
            });
}



applyStyle();

