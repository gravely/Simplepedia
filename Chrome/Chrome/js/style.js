function applyStyle() {
    console.log('Applying style.');
    // Grab localStorage, make this generic soonish
    chrome.extension.sendRequest({method: "copy_font"}, function(response) {
        localStorage['copy_font'] = response.status;
        console.log('got copy font: ' + response.status);
    });

    chrome.extension.sendRequest({method: "heading_font"}, function(response) {
        localStorage['heading_font'] = response.status;
        console.log('got headings font: ' + response.status);
    });

    chrome.extension.sendRequest({method: "bg_color"}, function(response) {
        localStorage['bg_color'] = response.status;
        console.log('got bg color: ' + response.status);
    });

    chrome.extension.sendRequest({method: "text_color"}, function(response) {
        localStorage['text_color'] = response.status;
        console.log('got text color: ' + response.status);
    });

    chrome.extension.sendRequest({method: "headings_color"}, function(response) {
        localStorage['headings_color'] = response.status;
        console.log('got headsing color: ' + response.status);
    });

    chrome.extension.sendRequest({method: "link_color"}, function(response) {
        localStorage['link_color'] = response.status;
        console.log('got link color: ' + response.status);
    });

    chrome.extension.sendRequest({method: "edit"}, function(response) {
        localStorage['edit'] = response.status;
        console.log('edit: ' + response.status);
    });

    chrome.extension.sendRequest({method: "international"}, function(response) {
        localStorage['international'] = response.status;
        console.log('got international: ' + response.status);
        console.log(localStorage['international']);
    });


    // Insert prefered styles
    if (localStorage['copy_font']) {
        try {
            $('body').css('font-family', localStorage['copy_font']);
        }
        catch(err) {
            console.log('Error setting copy font: ' +  err);
        }
    }
    else{}

    if (localStorage['heading_font']) {
        try {
            $('h1,h2,h3,h4,h5#mp-tfp-h2,.mp-header,#mp-tfp-h2,.mw-headline,#frontlogo,#firstHeading,.firstHeading').css('font-family', localStorage['heading_font']);
        }
        catch(err) {
            console.log('Error setting heading fonts: ' + err);
        }
    }

    // Add bg color
    if (localStorage['bg_color']) {
        try {
            $('body').css('background-color', localStorage['bg_color']);
        }
        catch(err) {
            console.log('Error setting bg color: ' + err);
        }
    }
    // Add text colors
    if (localStorage['text_color']) {
        try {
            $('body').css('color', localStorage['text_color']);
        }
        catch(err) {
            console.log('Error setting text colors: ' + err);
        }
    }

    // Add headings colors
    if (localStorage['headings_color']) {
        try {
            $('h1,h2,h3,h4,h5#mp-tfp-h2,.mp-header,#mp-tfp-h2,.mw-headline,#frontlogo,#firstHeading,.firstHeading').css('color', localStorage['headings_color']);
        }
        catch(err) {
            console.log('Error setting text colors: ' + err);
        }
    }


    // Add link colors
    if (localStorage['link_color']) {
        try {
            $('.toctext,.toclinks,.toclevel-1,a,a:link,a:active').css('color', localStorage['link_color']);
        }
        catch(err) {
            console.log('Error setting link colors: ' + err);
        }
    }
     // Add link colors
    if (localStorage['visited_color']) {
        try {
            $('a:visited').css('color', localStorage['visited_color']);
        }
        catch(err) {
            console.log('Error setting link colors: ' + err);
        }
    }
    
    // Show edit links 
    if (localStorage['edit'] != false) {
        try {
            var css = localStorage['edit_selectors'] + '{display:inline;}';
            $('head').append('<style>' + css + '</style>');
        }
        catch(err) {
            console.log('Error revealing logged-in edit links: ' + err);
        }
    }

    if (localStorage['international']) {
        try {
            var css = '#langJump{display:block;}';
            $('head').append('<style>' + css + '</style>');
        }
        catch(err) {
            console.log('Error revealing language selctor: ' + err);
        }
    }
    // All of our changes have been made, now undo the display: none on body 
    $('body').css('display', 'block');
}
