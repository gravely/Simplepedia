#!/bin/sh
# begin a build
cp ./src/simplepedia.js ./build/simplepedia.js
# import chunks of css 
headingscss=`cat ./src/css/headings.css  | tr -d "\n "`
linkcss=`cat ./src/css/link.css | tr -d "\n "`
maincss=`cat ./src/css/main.css | tr -d "\n "`
textcss=`cat ./src/css/text.css | tr -d "\n "`
usercss=`cat ./src/css/user.css | tr -d "\n "`
favicon=`uuencode -m ./src/images/favicon.png ./ | grep -v begin | tr -d "\n"`
# sed -i gnu vs. bsd is silly, ignoring it
# using & for / as it's not a valid base64 character
# and shouldn't show up in css
sed \
-e "s&%HEADINGSCSS%&$headingscss&" \
-e "s&%LINKCSS%&$linkcss&" \
-e "s&%MAINCSS%&$maincss&" \
-e "s&%TEXTCSS%&$textcss&" \
-e "s&%USERCSS%&$usercss&" \
-e "s&%FAVICON%&$favicon&" \
< ./build/simplepedia.js \
> ./build/tmp$$ && mv ./build/tmp$$ ./build/simplepedia.js
