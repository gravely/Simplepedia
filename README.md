Simplepedia
===========
Simplepedia was a [Greasemonkey userscript](http://archives.grantstavely.com/simplepedia-wiki/) I developed in early 2009 that made mediawikis a little easier on the eyes. I last updated the [userscripts.org version](http://userscripts.org/scripts/show/42312) in July 2010, and it is currently listed as having over 25,000 installs.

I'm glad thousands of people tried and hopefully enjoyed Simplepedia! Alas, I no longer use a greasemonkey compatible browser, and no longer maintain the userscript.

In 2011 I ported it to Safari, and the compiled extension is available [here on github](https://github.com/grantstavely/Simplepedia/blob/master/Wikipedia%20Reader.safariextension/Wikipedia%20Reader.safariextz?raw=true).

And then in 2012 I ported it to Chrome and that compiled extension is available [here on github](https://github.com/grantstavely/Simplepedia/blob/master/Chrome/WikipediaReader.crx?raw=true).

Ongoing Development
-------------------
Both of them worked when written (mostly), but both are currently broken because of changes to both browsers =/.

I also haven't kept either of them current with Wikipedia html and css changes so what little of them does work doesn't work on the main page of wikipedia, the Chrome extension can't load it's own preferences, etc. It probably wouldn't take much work to get either of them working properly, and I might get motivated to do it myself. If I do, I'll publish them here.

I'd be happy to accept a pull-request on any of the above / you're welcome to fork and publish it yourself.

All of the code in this repository is (CC) Attribution Non-Commercial Share Alike; [http://creativecommons.org/licenses/by-nc-sa/3.0](http://creativecommons.org/licenses/by-nc-sa/3.0)

Simplepedia Alternatives
-------------------------
* Safari has a native reader-mode.
* [Evernote Clearly](http://evernote.com/clearly/) is also nice.
* [Wikiepedia Mobile](http://en.m.wikipedia.org/wiki/Main_Page) is a so-so compromise.
