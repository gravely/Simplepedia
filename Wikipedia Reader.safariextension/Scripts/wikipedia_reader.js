function applyStyle(settings) {
    scrub();
    if (location.href.match(/http:\/\/?(www\.|)wikipedia\.org\//i)) {
        wwwWikipediaOrg();
        style(settings);
    }
    else {
        unWTF();
        addSearch();
        addLanguageJump();
        userHints(settings.user);
        style(settings);
    }
}

function scrub()
{
    // Remove all existing stylesheets
    $('link[rel="stylesheet"]').remove();
    $('style').remove();

    // Remove all inline style with some exceptions
    $('*:not(.thumbinner,.toccolours,.toccolours div,.roccolours,.roccolours div)').removeAttr('style');

    // Remove element attributes that change appearance
    $('*').removeAttr('color');
    $('*').removeAttr('width');
    $('*').removeAttr('height');
    $('*').removeAttr('font');

    // Remove non-content and competing scripts
    $('script').remove();
    $('iframe').remove();
    $('.suggestions').remove();
    $('#mw-js-message').remove();
    $('#sitenotice').remove();
    $('#mw-panel').remove();
    $('#left-navigation').remove();
    $('#right-navigation').remove();
    $('#footer').remove();
    $('#siteSub').remove();
    $('#sitesub').remove();
    $('#jump-to-nav').remove();
    $('.printfooter').remove();
    $('.printonly').remove();
    $('.internal').remove();
    $('#coordinates').remove();
    $('.collapseButton').remove();
    $('.mbox-image').remove();
    $('.protected-icon').remove();
    $('.topicon').remove();
    $('#mw-mf-page-left').remove();
    $('#page-actions').remove();
    $('.search-box').remove();
    $('#mw-hidden-catlinks').remove();
    $('#mw-navigation').remove();
    $('.metadata').remove();


    // If it shouldn't be printed it shouldn't be read
    $('.noprint').remove();
    // oddballs
    $('img[title="close"]').remove();
}

function wwwWikipediaOrg()
{
    $('.search-form').addClass('front');
    var search = $('.search-form')[0].outerHTML;
    var newFrontPage = document.createElement("body");
    var mastHead = '<title class="front">Wikipedia</title>';
    newFrontPage.innerHTML = mastHead + search;
    try {
        $('body').replaceWith($(newFrontPage));
        $('#searchInput').after('\
            <br />\
            <form id="random-article" action="/wiki/Special:Random">\
                <input type="submit" name="random" value="?" />\
            </form>');
    }
    catch(err) {console.log(err);}
}

function unWTF()
{
    // in which we directly attack specific popular wikimedia templates, for lack of a
    // better generic strategy:
    // Edit links: "[" <- really?
    try {
        var editsections = ($('span.editsection').get());
        for (var i = 0; i < editsections.length; i++) {
            var links = ($('a', editsections[i]).get());
            for (var j = 0; j < links.length; j++) {
                $(editsections[i]).replaceWith('<a class="editsection" href="' + $(links[j]).attr('href') + '">✎</a>');
            }
        }
    }
    catch(err) {console.log(err);}

    // Eliminate anything similar to cquote. This entire script could be called cquote.
    // cquote is everything wrong with the style of wikipedia in one, ugly, terrible
    // template. So. Ugly.
    try {
        var cells = ($('td').get());
        for (var i = 0; i < cells.length; i++) {
            if ($(cells[i]).text() == '“') {
                // AH HA! A Table cell that only exists to add a purple curly quote!
                console.log('Replacing a cquote with a blockquote containing: ' + $(cells[i + 1]).text());
                // replace the monster's parent table, thus restoring sanity to the world,
                // with a blockquote containing the next td's content
                $(cells[i]).closest('table').replaceWith('<blockquote>' + $(cells[i + 1]).text() + '</blockquote>');
            }
            //alert("Orchestra hit!");
        }
    }
    catch(err) {console.log(err);}

    // clean up reference sups
    try {
        var spans = ($('a span').get());
        for (var i = 0; i < spans.length; i++) {
            if ($(spans[i]).text() == '[' || ']') {
                // AH HA! A Table cell that only exists to add a purple curly quote!
                //console.log('Fixing reference sups for ref: ' + $(spans[i + 1]).text());
                $(spans[i]).text('');
            }
        }
        var sups = ($('sup a').get());
        for (var i = 0; i < sups.length; i++) {
            var sup = $(sups[i]).text();
            sup.replace(']', '');
            sup.replace('[', '');
            $(sups[i]).text(sup);
        }
    }
    catch(err) {console.log(err);}

    // nix the hard-coded css columns for references: ugh
    try {
        $('.reflist').removeAttr("style");
        $('.references-column-width').removeAttr("style");
    }
    catch(err) {console.log(err);}

}

function addSearch()
{
    //
    // Search using whatever search form the page already has
    try {
        // so's we stay on the same language
        var searchform = $('#searchform').attr('action');
            // build it
            var searchbar = document.createElement('div');
            searchbar.innerHTML='\
                    <form id="search" action="' + searchform + '" method="get">\
                            <div id="search_bar">\
                                <input name="title" type="hidden" value="Special:Search" />\
                                <input name="ns0" type="hidden" value="1" />\
                                <label for="searchinput">Search:</label>\
                                <input id="searchInput" name="search" type="text" name="searchinput" title="Search Wikipedia [f]" accesskey="f" results="10" value="" />\
                                <br />\
                             </form>\
                            </div>\
                    </form>';
            searchbar.id="floatingsearch";
            $('#bodyContent').append($(searchbar));
    }
    catch(err) {console.log(err);}
}

function addLanguageJump()
{
    // Jump to the same article in other languages
    if (localStorage['international']) {
        try {
            var langList = document.getElementById("p-lang").getElementsByTagName("a");
            if (langList.length > 0) {
                var langJump = document.createElement("select");
                langJump.id = "langJump";
                langJump.name = "langJump";
                var langTitle = document.createElement("option");
                // snag 'in another language' in the local translation
                langTitle.text = document.getElementById("p-lang").getElementsByTagName('h5')[0].innerHTML;
                langJump.add(langTitle,null);
                for (var i = 0; i <= langList.length -1; i++) {
                    var langOpt = document.createElement("option");
                    langOpt.text = langList[i].innerHTML;
                    langOpt.value = langList[i];
                    try {
                        langJump.add(langOpt,null);
                    }
                catch(err) {console.log(err);}
                }
                langJump.addEventListener("change", function() {
                    window.location.href = this.options[this.selectedIndex].value;
                }, false);
                try {
                    document.getElementById("content").insertBefore(langJump);
                }
                catch(err) {console.log(err);}
            }
        }
        catch(err) {console.log('Unable to find this article in any other lanuages.');}
    }
}

function userHints(userSetting)
{
    if (userSetting === false) {
            $('#p-personal').remove();
    }
    else {
        try {
        // pt-userpage is only there if already logged in
        var userNav = document.getElementById("pt-userpage");
        userNav.parentNode.setAttribute("id","user-ul");
        // when not, use pt-login
        var userLogin = document.getElementById("pt-login");
        userLogin.parentNode.parentNode.setAttribute("id","login");
        userLogin.parentNode.parentNode.setAttribute("class","");
    }
    catch(err) {console.log('You don\'t appear to be logged into Wikipedia.');}
    }

}

function style(settings) {
    // Insert prefered styles
    if (settings.copy_font) {
        try {
            $('body').css('font-family', settings.copy_font);
        }
        catch(err) {
            console.log('Error setting copy font: ' +  err);
        }
    }
    else{}

    if (settings.heading_font) {
        try {
            $('h1,h2,h3,h4,h5#mp-tfp-h2,.mp-header,#mp-tfp-h2,.mw-headline,#frontlogo,#firstHeading,.firstHeading').css('font-family', settings.heading_font);
        }
        catch(err) {
            console.log('Error setting heading fonts: ' + err);
        }
    }

    // Add bg color
    if (settings.bg_color) {
        try {
            $('body').css('background-color', settings.bg_color);
        }
        catch(err) {
            console.log('Error setting bg color: ' + err);
        }
    }
    // Add text colors
    if (settings.text_color) {
        try {
            $('body').css('color', settings.text_color);
        }
        catch(err) {
            console.log('Error setting text colors: ' + err);
        }
    }

    // Add headings colors
    if (settings.headings_color) {
        try {
            $('h1,h2,h3,h4,h5#mp-tfp-h2,.mp-header,#mp-tfp-h2,.mw-headline,#frontlogo,#firstHeading,.firstHeading').css('color', settings.headings_color);
        }
        catch(err) {
            console.log('Error setting text colors: ' + err);
        }
    }


    // Add link colors
    if (settings.link_color) {
        try {
            $('.toctext,.toclinks,.toclevel-1,a,a:link,a:active').css('color', settings.link_color);
        }
        catch(err) {
            console.log('Error setting link colors: ' + err);
        }
    }
     // Add link colors
    if (settings.visited_color) {
        try {
            $('a:visited').css('color', settings.visited_color);
        }
        catch(err) {
            console.log('Error setting link colors: ' + err);
        }
    }

    // Show edit links
    if (settings.edit === false) {
            $('.mw-editsection').remove();
    }

    if (settings.international) {
        try {
            var international_css = '#langJump{display:block;}';
            $('head').append('<style>' + international_css + '</style>');
        }
        catch(err) {
            console.log('Error revealing language selctor: ' + err);
        }
    }
    // All of our changes have been made, now undo the display: none on body
    $('body').css('display', 'block');
}

