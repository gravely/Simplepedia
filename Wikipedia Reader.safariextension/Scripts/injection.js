if (window == window.parent) {
    safari.self.addEventListener('message', handleMessage, false);
    safari.self.tab.dispatchMessage('applyStyle');
}

function handleMessage(event)
{
    if (event.name == 'applyStyle') {
        console.log('Applying Wikipedia Reader style to ' + document.location);
        settings = event.message;
        applyStyle(settings);
    }
}